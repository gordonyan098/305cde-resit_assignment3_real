var myApp = angular.module('myApp', []);
myApp.controller('ClaimMoney', function($scope, $http){
    var url = "https://gordonliveapi.herokuapp.com/";
    var login ="user";
    $scope.my = {name:''}

    $scope.init = function(){
        $scope.list();
    }

    $scope.login = function(){
        login = $scope.LoginUser;
        $scope.my.name = $scope.LoginUser;
        $scope.LoginPass = null ;
        $scope.LoginUser = null ;
        
    }

    $scope.logout = function(){
        login ="user";
        $scope.my.name = "";
    }

    $scope.Addform = function(){
        document.getElementById("add_record").classList.remove("ng-hide");
        document.getElementById("claimmoneyrecord").classList.add("ng-hide");
        document.getElementById("edit_form").classList.add("ng-hide");  
    }


    $scope.home = function(){
        document.getElementById("add_record").classList.add("ng-hide");
        document.getElementById("claimmoneyrecord").classList.remove("ng-hide");
        document.getElementById("edit_form").classList.add("ng-hide");    
    }

    $scope.list = function(){
        $http({
            method:"get",
            url: url +"alldata"
        }).then(function Successful(response){
            console.log(response.data.Data);

            $scope.dates = response.data.Data;
            $scope.length = response.data.Count;
        })
    }
    
	$scope.totalcost = function(){
		if(login == "user"){

		}else{
		$http({
			method:"get",
			url: url+"finddayrecord?id="+$(e.target).data('Date')
		}).then(function Successful(response){
            console.log(response.data);
            $scope.totalcosts = response.data.Totalcost;
            });         

		}
	}
    
    


    $scope.adds = function(){
		if(login == "user"){

		}else{
		var claimdata = {
            "Bib"  : $scope.add.Bib,
            "Location"  : $scope.add.Location,
            "Category"  : $scope.add.Category,
            "Consumption"  : $scope.add.Consumption
             
        };
        $scope.add.Bib='';
        $scope.add.Location='';
        $scope.add.Category='';
        $scope.add.Consumption='';

		$http({
			method:"post",
			url: url+"input",
			data: JSON.stringify(claimdata)
		}).then(function Successful(response){
			console.log(response.data);
			if(response.data.Status == "You had add a record!"){
				document.getElementById("add_record").classList.add("ng-hide");
                document.getElementById("claimmoneyrecord").classList.remove("ng-hide");
				$scope.list();
			}
			
		})
	}
    }
    
    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.length; i++){
            var product = $scope.dates[i];
            total += (product.Consumption);
        }
        return total;
    }

    $scope.get = function(e){
		if(login == "user"){

		}else{
		$http({
			method:"get",
			url: url+"get?id="+$(e.target).data('id')
		}).then(function Successful(response){
			console.log(response.data);
			
            $scope.Edit.id = response.data.Data[0]._id;
            $scope.Edit.Bib = response.data.Data[0].Bib;
            $scope.Edit.date = response.data.Data[0].date;
            $scope.Edit.Location = response.data.Data[0].Location;
            $scope.Edit.Category = response.data.Data[0].Category;
            $scope.Edit.Consumption = response.data.Data[0].Consumption;

            //document.getElementById("add_record").classList.add("ng-hide");
            document.getElementById("claimmoneyrecord").classList.add("ng-hide");
			document.getElementById("edit_form").classList.remove("ng-hide");
		})
	}
	}

    $scope.Edit = function(){
        
		var claimdata = {

            "Bib"  : $scope.Edit.Bib,
            "Location"  : $scope.Edit.Location,
            "Date"  : $scope.Edit.date,
            "Type"  : $scope.Edit.type,
            "Consumption"  : $scope.Edit.Consumption
             
		};
		console.log($scope.Edit.id);
		$http({
			method:"put",
			url: url+"modify?id="+$scope.Edit.id,
			data: JSON.stringify(claimdata)
		}).then(function Successful(response){
            console.log(response.data);
			if(response.data.Status == "Modify Successfull!"){
				document.getElementById("edit_form").classList.add("ng-hide");
				document.getElementById("claimmoneyrecord").classList.remove("ng-hide");
				$scope.list();
			}
		})
    }
    
    $scope.delete = function(e){
		if(login == "user"){

		}else{
		console.log($(e.target).data('id'));
		$http({
			method:"DELETE",
			url: url+"delete?id="+$(e.target).data('id')
		}).then(function Successful(response){
			console.log(response.data);
			if(response.data.Status == "Delete Successful"){
				$scope.list();
			}
		})
	}
	}

});

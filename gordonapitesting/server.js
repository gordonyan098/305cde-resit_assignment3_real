//RuningEvent system, they can buy some food and drink in each check point
//each checkpoint can save thier information, check point, bib, time, Consumption

//Connect to mongo db
var mongoose = require('mongoose');
mongoose.connect('mongodb://gordon01:gordon01@ds113046.mlab.com:13046/gordonyan');

var corsSetup = require("restify-cors-middleware");
var cors = corsSetup({
    origins: ["*"]
})

//defind item in database
var RuningEvent = mongoose.model('RuningEvent', {
    Date : String,
    Weather : String,
    Temp: Number,
    Bib : String,
    Location : String,
    Category : String,
    Consumption : Number
});


var getData = function (callback) {
    var weatherdata = "";

    var openweather = {
        hostname: "api.openweathermap.org",
        post: 80,
        path: "/data/2.5/weather?appid=70089dc4a17c1db186e6c9c04fa5ebf6&id=1819729&units=metric",
        method: 'GET',
    };

    var req = http.request(openweather, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (data) {
            weatherdata += data;
        }).on('end', function () {
            callback(JSON.parse(weatherdata));
        });
    });

    req.on('error', function (e) {
        console.log('The problem of the request is : ' + e.message);
    });
    req.end();
}

var restify = require('restify');
var http = require("http");
var server = restify.createServer();

server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.queryParser());
server.use(restify.plugins.jsonBodyParser());


//open server 
server.listen((process.env.PORT || 8080), function () {
    console.log("The server is using");
});


/*
server.listen(10023, function () {
    console.log("The port 10023 and server is using");
});
*/







//Get record from database
server.get("/alldata", function (req, res, next) {
    RuningEvent.find({}, function (error, data) {

        if (error) {
            res.send({
                Status: "Fail ",
                Message: error.messages
            });
        } else {
            res.send({
                Status: "Here is the record",
                Count: data.length,
                Data: data
            });
        }

        res.end();
    })
});

//Get the date with dd/mm/yy this format
var nowdate = new Date();
date1 = (nowdate.getDate() +"/" +(nowdate.getMonth()+1) +"/" +nowdate.getFullYear());
server.post("/input", function (req, res, next) {
    getData(function (result) {

        var checkdata = new RuningEvent({
            Date : date1,
            Weather : result.weather[0].main,
            Temp: result.main.temp,
            Bib : req.body.Bib,
            Location : req.body.Location,
            Category : req.body.Category,
            Consumption : req.body.Consumption
        });

        checkdata.save().then(() => {
            res.send({ Status: "You had add a record!" });
            res.end();
        });
    });
});

//Delete data
server.del("/delete", function(req, res, next){
    if(req.query != null && req.query.id != null){
        var deleteid = req.query.id;
        RuningEvent.findByIdAndRemove(deleteid,function(error,data){
            if(!error){
                res.send({
                    Status : "Delete Successful",
                    Message : "You had deleted the record!"
                });
            } else {
                res.send({
                    Status : "Delete Fail ",
                    Message : error.messages
                });
            }
            res.end();
        });
    } else {
        res.send({
            Status : "Fail",
            Message: "Please try again."
        });
        res.end();
    }

});

// Find record
server.get("/finddayrecord", function(req, res, next){
    if(req.query !=null && req.query.Date !=null ){
        var filterdate = req.query.Date;
        RuningEvent.find({Date : filterdate }, function(error, data){
        var newmoney = 0;
        var i = 0;
        data.forEach(element => {
               newmoney = newmoney+data[i].Consumption;
               i++;
            });
            console.log(newmoney);
            if (!error){
                res.send({
                    Status : "This data record are :",
                    Data :  data ,
                    Totalcost  : newmoney
                });
            }else{
                 res.send({
                     Status : "There are not record is matching the date",
                     Message: error.message
                 })   
                }
        
    });
    }
});


server.get("/get", function (req, res, next) {
    if (req.query != null && req.query.id != null) {
        RuningEvent.find({ _id: req.query.id }, function (error, data) {

            if (!error) {
                res.send({
                    Status: "success",
                    Count: data.length,
                    Data: data
                });
            } else {
                res.send({
                    Status: "fail",
                    Message: error.messages
                });
            }
            res.end();
        });
    } else {
        res.send({
            Status: "fail",
            Message: "please check request."
        });
        res.end();
    }

});


//Modify the data by using the id 
server.put("/modify", function (req,res, next) {
    //make sure you have input all record
    if(req.query !=null && req.query.id!=null && req.body!=null && req.body.Bib!=null && req.body.Location!=null && req.body.Category!=null && req.body.Consumption!=null){
        //get back the new record 
        var modifyid = req.query.id;
        var modifydate = req.query.Date;
        var modifybib = req.body.Bib;
        var modifyloc = req.body.Location;
        var modifycat =  req.body.Category;
        var modifymoney = req.body.Consumption;
        //using 'findByIdAndUpdate' to modify the data
        RuningEvent.findByIdAndUpdate(modifyid,{Bib : modifybib, Location : modifyloc, Category : modifycat, Consumption : modifymoney}, function(error, data){
            if(!error){
                //if success then show "You had Updated the record!"
                res.send({
                    Status : "Modify Successfull!",
                    Message : "You had Updated the record!"
                });
            }
            else {
                //if fail show error message
                res.send({
                    StatusModify : "Fail!" ,
                    Message : error.messages
                });
            }
            res.end();
        });
        
    }else{
            //if cannot find the match id will show "Please try again!"
            res.send({
                Status :  "Modify Fail!",
                Message : "Please try again!."
                });
        res.end();
    }

});

